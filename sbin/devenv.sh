#!/usr/bin/env zsh

export ENV_HOME=${HOME}/projects/env
 
 if [[ "$#" == "0" ]];then
    cd ${ENV_HOME}
    docker-compose up -d
    exit 0
fi

if [[ "$1" == "down" ]];then
    cd ${ENV_HOME}
    docker-compose down
    exit 0
fi


echo "unkown command: $1"



